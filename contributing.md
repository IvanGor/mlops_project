Let's first write something to apply linters on.

How to commit:

1. Install pre-commit
2. Clone repository
3. After installing pre-commit run in the repository "pre-commit install"
4. After changes please run git add -> git commit, and if there are any corrections from pre-commit hooks please re-run add and commit.
